import Foundation

struct SearchResponse {
    let query: String
    let nextPageToken: String?
    let totalCount: Int
    let items: [Item]
}

struct Item {
    let id: String
    let title: String
    let authors: [Author]
    let narrators: [Author]
    let cover: Cover
}

struct Author {
    let id: String
    let name: String
}

struct Cover {
    let url: URL?
    let width: Double
    let height: Double
}
