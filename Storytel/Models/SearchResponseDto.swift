import Foundation

struct SearchResponseDto: Decodable {
    let query: String
    let nextPageToken: String?
    let totalCount: Int
    let items: [ItemDto]
}

struct ItemDto: Decodable {
    let id: String
    let title: String
    let authors: [AuthorDto]
    let narrators: [AuthorDto]
    let cover: CoverDto
}

struct AuthorDto: Decodable {
    let id: String
    let name: String
}

struct CoverDto: Decodable {
    let url: String
    let width: Double
    let height: Double
}

extension SearchResponseDto {
    func toDomain() -> SearchResponse {
        .init(
            query: query,
            nextPageToken: nextPageToken,
            totalCount: totalCount,
            items: items.map { $0.toDomain() }
        )
    }
}

extension ItemDto {
    func toDomain() -> Item {
        .init(
            id: id,
            title: title,
            authors: authors.map { $0.toDomain() },
            narrators: narrators.map { $0.toDomain() },
            cover: cover.toDomain()
        )
    }
}

extension CoverDto {
    func toDomain() -> Cover {
        .init(
            url: URL(string: url),
            width: width,
            height: height
        )
    }
}

extension AuthorDto {
    func toDomain() -> Author {
        .init(id: id, name: name)
    }
}
