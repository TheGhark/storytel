import UIKit

final class LoadingView: UIView {
    // MARK: - Properties

    private let activityIndicator = UIActivityIndicatorView()

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHeriarchy()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension LoadingView {
    func setupViews() {
        backgroundColor = .lightGray
        activityIndicator.style = .gray
        activityIndicator.startAnimating()
    }

    func setupHeriarchy() {
        addSubview(activityIndicator)
    }

    func setupConstraints() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.pinToSuperviewEdges()
    }
}
