import SDWebImage
import UIKit

final class SearchResultCell: UITableViewCell {
    // MARK: - Properties

    static let reusableIdentifier = String(describing: SearchResultCell.self)
    private let coverImageView = UIImageView()
    private let titleLabel = UILabel()
    private let authorsLabel = UILabel()
    private let narratorsLabel = UILabel()
    private let stackView = UIStackView()

    // MARK: - Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupHierarchy()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Internal

    func update(with model: SearchResultModel) {
        titleLabel.text = model.title
        authorsLabel.text = model.authors
        narratorsLabel.text = model.narrators
        coverImageView.sd_setImage(with: model.imageUrl, completed: nil)
    }
}

private extension SearchResultCell {
    func setupViews() {
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 4

        titleLabel.font = .boldSystemFont(ofSize: UIFont.labelFontSize)
        titleLabel.numberOfLines = 1

        authorsLabel.numberOfLines = 1
        narratorsLabel.numberOfLines = 1
    }

    func setupHierarchy() {
        addSubview(coverImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(authorsLabel)
        stackView.addArrangedSubview(narratorsLabel)
        addSubview(stackView)
    }

    func setupConstraints() {
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.pinToSuperviewEdges([.left, .top, .bottom])
        coverImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.pinToSuperviewEdges([.top, .right, .bottom])

        stackView.leftAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: 4).isActive = true
    }
}
