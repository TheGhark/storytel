import UIKit

final class HeaderView: UIView {
    // MARK: - Properties

    private let label = UILabel()

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHeriarchy()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Internal

    func update(with text: String) {
        label.text = text
    }
}

private extension HeaderView {
    func setupViews() {
        backgroundColor = .lightGray
        label.font = .systemFont(ofSize: 24)
        label.textAlignment = .center
    }

    func setupHeriarchy() {
        addSubview(label)
    }

    func setupConstraints() {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.pinToSuperviewEdges()
    }
}
