import UIKit

final class SearchViewController: UITableViewController {
    // MARK: - Properties

    private let viewModel: SearchViewModel
    private let headerView = HeaderView(
        frame: .init(
            origin: .zero,
            size: .init(width: UIScreen.main.bounds.width, height: 200)
        )
    )

    // MARK: - Initialization

    init(viewModel: SearchViewModel = .init()) {
        self.viewModel = viewModel
        super.init(style: .plain)
        viewModel.delegate = self
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UITableViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        viewModel.search()
        navigationController?.navigationBar.isHidden = true
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfItems
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.model(at: indexPath.row)
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: SearchResultCell.reusableIdentifier,
            for: indexPath
        ) as? SearchResultCell
        else {
            fatalError(
                "`SearchResultCell` must be registered to be dequeued for `\(SearchResultCell.reusableIdentifier)`"
            )
        }
        cell.update(with: model)
        return cell
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0
            && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        tableView.tableFooterView?.isHidden = !isReachingEnd

        if isReachingEnd {
            viewModel.search()
        }
    }
}

private extension SearchViewController {
    func setupViews() {
        tableView.register(
            SearchResultCell.self,
            forCellReuseIdentifier: SearchResultCell.reusableIdentifier
        )
        tableView.allowsSelection = false
        tableView.rowHeight = 80
        tableView.tableFooterView = LoadingView(
            frame: .init(
                origin: .zero,
                size: .init(width: UIScreen.main.bounds.width, height: 80)
            )
        )
        tableView.tableFooterView?.isHidden = true
        tableView.tableHeaderView = headerView
    }
}

extension SearchViewController: SearchViewModelDelegate {
    func reload() {
        headerView.update(with: viewModel.formattedQuery)
        tableView.reloadData()
    }
}
