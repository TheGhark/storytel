import Foundation

protocol SearchRepositoryProtocol {
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponse, EndpointError>) -> Void
    )
}

final class SearchRepository {
    // MARK: - Properties

    private let service: SearchApiServiceProtocol

    // MARK: - Initialization

    init(service: SearchApiServiceProtocol = SearchApiService()) {
        self.service = service
    }
}

extension SearchRepository: SearchRepositoryProtocol {
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponse, EndpointError>) -> Void
    ) {
        service.search(query: query, nextPageToken: nextPageToken) { result in
            switch result {
            case let .success(dto):
                completion(.success(dto.toDomain()))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
