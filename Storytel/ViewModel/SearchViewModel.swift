import Foundation

protocol SearchViewModelDelegate: class {
    func reload()
}

final class SearchViewModel {
    // MARK: - Properties

    weak var delegate: SearchViewModelDelegate?
    private var models: [SearchResultModel] = []
    private var query: String = ""
    private var nextPageToken: String?
    private let repository: SearchRepositoryProtocol
    private let modelFactory: SearchModelFactoryProtocol

    // MARK: - Computed Properties

    var formattedQuery: String {
        "Querry: \(query)"
    }

    var numberOfItems: Int {
        models.count
    }

    // MARK: - Initialization

    init(
        repository: SearchRepositoryProtocol = SearchRepository(),
        modelFactory: SearchModelFactoryProtocol = SearchModelFactory()
    ) {
        self.repository = repository
        self.modelFactory = modelFactory
    }

    // MARK: - Internal

    func model(at index: Int) -> SearchResultModel {
        models[index]
    }

    func search(query: String = "harry") {
        self.query = query
        repository.search(query: query, nextPageToken: nextPageToken) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                self.nextPageToken = response.nextPageToken
                self.updateModels(with: response)
                DispatchQueue.main.async {
                    self.delegate?.reload()
                }
            case .failure:
                DispatchQueue.main.async {
                    self.delegate?.reload()
                }
            }
        }
    }
}

private extension SearchViewModel {
    func updateModels(with response: SearchResponse) {
        let new = response.items.map { modelFactory.model(with: $0) }
        models.append(contentsOf: new)
    }
}
