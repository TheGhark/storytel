import Foundation

struct SearchResultModel: Equatable {
    let imageUrl: URL?
    let title: String
    let authors: String
    let narrators: String
}

protocol SearchModelFactoryProtocol {
    func model(with item: Item) -> SearchResultModel
}

final class SearchModelFactory {}

// MARK: - Private

private extension SearchModelFactory {
    func authors(with authors: [Author]) -> String {
        let description = authors
            .map { $0.name }
            .joined(separator: ",")
        return "by \(description)"
    }

    func narrators(with narrators: [Author]) -> String {
        let description = narrators
            .map { $0.name }
            .joined(separator: ",")
        return "with \(description)"
    }
}

// MARK: - SearchModelFactoryProtocol

extension SearchModelFactory: SearchModelFactoryProtocol {
    func model(with item: Item) -> SearchResultModel {
        .init(
            imageUrl: item.cover.url,
            title: item.title,
            authors: authors(with: item.authors),
            narrators: narrators(with: item.narrators)
        )
    }
}
