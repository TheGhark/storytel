import Foundation

enum Endpoint {
    case search

    var path: String {
        switch self {
        case .search:
            return "/search"
        }
    }
}

enum EndpointError: Error {
    case invalidUrl
    case serverError(error: Error)
    case noData
    case unknown
    case cannotParse
}
