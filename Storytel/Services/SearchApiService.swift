import Foundation

protocol SearchApiServiceProtocol {
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponseDto, EndpointError>) -> Void
    )
}

final class SearchApiService {
    // MARK: - Properties

    private let decoder: JSONDecoder
    private let session: URLSession
    private let baseUrlPath: String

    // MARK: - Initialization

    init(
        decoder: JSONDecoder = .init(),
        session: URLSession = .shared,
        baseUrlPath: String = BaseURL.base
    ) {
        self.decoder = decoder
        self.session = session
        self.baseUrlPath = baseUrlPath
    }
}

extension SearchApiService: SearchApiServiceProtocol {
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponseDto, EndpointError>) -> Void
    ) {
        let urlPath = baseUrlPath.appending(Endpoint.search.path)
        let queryItems: [URLQueryItem] = [
            .init(name: "query", value: query),
            .init(name: "page", value: nextPageToken)
        ]
        var urlComponents = URLComponents(string: urlPath)
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else {
            completion(.failure(.invalidUrl))
            return
        }
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) { [weak self] data, _, error in
            guard let self = self else {
                completion(.failure(.unknown))
                return
            }
            if let error = error {
                completion(.failure(.serverError(error: error)))
            } else if let data = data {
                do {
                    let searchResponse = try self.decoder.decode(SearchResponseDto.self, from: data)
                    completion(.success(searchResponse))
                } catch {
                    completion(.failure(.cannotParse))
                }
            } else {
                completion(.failure(.noData))
            }
        }
        task.resume()
    }
}
