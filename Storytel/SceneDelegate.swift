//
//  SceneDelegate.swift
//  Storytel
//
//  Created by Camilo Rodriguez Gaviria on 24/04/2021.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        let searchViewController = SearchViewController()
        let navigationViewController = UINavigationController(rootViewController: searchViewController)
        window?.rootViewController = navigationViewController
        window?.makeKeyAndVisible()
    }
}
