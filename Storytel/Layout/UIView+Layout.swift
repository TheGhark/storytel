import UIKit

extension UIView {
    func pinToSuperviewEdges() {
        guard let superview = superview else { return }
        leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
        topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
    }

    func pinToSuperviewEdges(_ edges: [UIRectEdge]) {
        guard let superview = superview else { return }

        edges.forEach { edge in
            switch edge {
            case .left:
                leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            case .top:
                topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            case .right:
                rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            case .bottom:
                bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            default:
                break
            }
        }
    }
}
