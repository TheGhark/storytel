@testable import Storytel

final class MockFunc<Input, Output> {
    var parameters: [Input] = []
    var result: (Input) -> Output = { _ in fatalError() }
    var completions: [(Output) -> Void] = []
    var called: Bool = false
    var count: Int = 0

    func returns(_ value: Output) {
        result = { _ in value }
    }

    func call(_ input: Input) {
        parameters.append(input)
    }

    func callNow(_ input: Input) -> Output {
        parameters.append(input)
        return result(input)
    }

    func callAndReturn(_ input: Input, completion: @escaping (Output) -> Void) {
        call(input)
        completions.append(completion)
        completion(result(input))
    }
}

final class SearchApiServiceMock: SearchApiServiceProtocol {
    var searchMockFunc = MockFunc<Void, Result<SearchResponseDto, EndpointError>>()
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponseDto, EndpointError>) -> Void
    ) {
        searchMockFunc.callAndReturn((), completion: completion)
    }
}

final class SearchRepositoryMock: SearchRepositoryProtocol {
    var searchMockFunc = MockFunc<Void, Result<SearchResponse, EndpointError>>()
    func search(
        query: String,
        nextPageToken: String?,
        completion: @escaping (Result<SearchResponse, EndpointError>) -> Void
    ) {
        searchMockFunc.callAndReturn((), completion: completion)
    }
}

final class SearchModelFactoryMock: SearchModelFactoryProtocol {
    var modelMockFunc = MockFunc<Void, SearchResultModel>()
    func model(with item: Item) -> SearchResultModel {
        modelMockFunc.callNow(())
    }
}
