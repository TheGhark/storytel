import XCTest
@testable import Storytel

final class SearchRepositoryTests: XCTestCase {
    var sut: SearchRepository!
    var service: SearchApiServiceMock!

    override func setUp() {
        super.setUp()
        service = .init()
        sut = .init(service: service)
    }

    override func tearDown() {
        service = nil
        sut = nil
        super.tearDown()
    }

    func test_search_success() {
        service.searchMockFunc.returns(.success(.sample()))
        let expectation = self.expectation(description: #function)
        sut.search(query: "abcde", nextPageToken: "20") { result in
            switch result {
            case let .success(response):
                XCTAssertFalse(response.items.isEmpty)
            case .failure:
                XCTFail("This request should not fail")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.1)
    }

    func test_search_failure() {
        service.searchMockFunc.returns(.failure(.noData))
        let expectation = self.expectation(description: #function)
        sut.search(query: "abcde", nextPageToken: "20") { result in
            if case .success = result {
                XCTFail("This request should fail")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.1)
    }
}
