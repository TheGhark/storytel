import Foundation
@testable import Storytel

extension SearchResultModel {
    static func sample(
        imageUrl: URL? = nil,
        title: String = "title",
        authors: String = "authors",
        narrators: String = "narrators"
    ) -> SearchResultModel {
        .init(
            imageUrl: imageUrl,
            title: title,
            authors: authors,
            narrators: narrators
        )
    }
}
