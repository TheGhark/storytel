import Foundation
@testable import Storytel

extension SearchResponseDto {
    static func sample(
        query: String = "harry",
        nextPageToken: String? = "20",
        totalCount: Int = 1000,
        items: [ItemDto] = [.sample()]
    ) -> SearchResponseDto {
        .init(
            query: query,
            nextPageToken: nextPageToken,
            totalCount: totalCount,
            items: items
        )
    }
}

extension ItemDto {
    static func sample(
        id: String = UUID().uuidString,
        title: String = "title",
        authors: [AuthorDto] = [.sample()],
        narrators: [AuthorDto] = [.sample()],
        cover: CoverDto = .sample()
    ) -> ItemDto {
        .init(
            id: id,
            title: title,
            authors: authors,
            narrators: narrators,
            cover: cover
        )
    }
}

extension CoverDto {
    static func sample(
        url: String = "https://www.storytel.com/images/9781781102404/640x640/cover.jpg",
        width: Double = 640,
        height: Double = 640
    ) -> CoverDto {
        .init(url: url, width: width, height: height)
    }
}

extension AuthorDto {
    static func sample(
        id: String = UUID().uuidString,
        name: String = "Name"
    ) -> AuthorDto {
        .init(id: id, name: name)
    }
}
