import Foundation
@testable import Storytel

extension SearchResponse {
    static func sample(
        query: String = "harry",
        nextPageToken: String? = "20",
        totalCount: Int = 1000,
        items: [Item] = [.sample()]
    ) -> SearchResponse {
        .init(
            query: query,
            nextPageToken: nextPageToken,
            totalCount: totalCount,
            items: items
        )
    }
}

extension Item {
    static func sample(
        id: String = UUID().uuidString,
        title: String = "title",
        authors: [Author] = [.sample()],
        narrators: [Author] = [.sample()],
        cover: Cover = .sample()
    ) -> Item {
        .init(
            id: id,
            title: title,
            authors: authors,
            narrators: narrators,
            cover: cover
        )
    }
}

extension Cover {
    static func sample(
        url: String = "https://www.storytel.com/images/9781781102404/640x640/cover.jpg",
        width: Double = 640,
        height: Double = 640
    ) -> Cover {
        .init(url: URL(string: url), width: width, height: height)
    }
}

extension Author {
    static func sample(
        id: String = UUID().uuidString,
        name: String = "Name"
    ) -> Author {
        .init(id: id, name: name)
    }
}
