import XCTest
@testable import Storytel

final class SearchViewModelTests: XCTestCase {
    var sut: SearchViewModel!
    var repository: SearchRepositoryMock!
    var modelFactory: SearchModelFactoryMock!

    var reloadCount = 0
    var reloadHandler: (() -> Void)?

    override func setUp() {
        super.setUp()
        repository = .init()
        modelFactory = .init()
        sut = .init(
            repository: repository,
            modelFactory: modelFactory
        )
        sut.delegate = self
    }

    override func tearDown() {
        modelFactory = nil
        repository = nil
        sut = nil
        super.tearDown()
    }

    func test_search() {
        repository.searchMockFunc.returns(.success(.sample()))
        modelFactory.modelMockFunc.returns(.sample())
        let expectation = self.expectation(description: #function)
        reloadHandler = { [weak sut] in
            XCTAssertEqual(sut?.numberOfItems, 1)
            expectation.fulfill()
        }
        sut.search()
        wait(for: [expectation], timeout: 0.1)
    }

    func test_search_failure() {
        repository.searchMockFunc.returns(.failure(.unknown))
        let expectation = self.expectation(description: #function)
        reloadHandler = { [weak sut] in
            XCTAssertEqual(sut?.numberOfItems, 0)
            expectation.fulfill()
        }
        sut.search()
        wait(for: [expectation], timeout: 0.1)
    }

    func test_search_multiple() {
        repository.searchMockFunc.returns(.success(.sample()))
        modelFactory.modelMockFunc.returns(.sample())
        let expectation = self.expectation(description: #function)
        reloadHandler = { [weak self] in
            if self?.reloadCount == 2 {
                XCTAssertEqual(self?.sut.numberOfItems, 2)
                expectation.fulfill()
            }
        }
        sut.search()
        sut.search()
        wait(for: [expectation], timeout: 0.1)
    }

    func test_search_resultModel() {
        let item: Item = .sample(
            title: "Harry Potter and the Order of the Phoenix",
            authors: [.sample(name: "J.K. Rowlin")],
            narrators: [.sample(name: "Stephen Fry")],
            cover: .sample(url: "https://www.storytel.com/images/9781781102404/640x640/cover.jpg")
        )
        let expected = SearchResultModel(
            imageUrl: item.cover.url,
            title: item.title,
            authors: "by J.K. Rowlin",
            narrators: "with Stephen Fry"
        )
        repository.searchMockFunc.returns(.success(.sample(items: [item])))
        modelFactory.modelMockFunc.returns(expected)
        let expectation = self.expectation(description: #function)
        reloadHandler = { [weak sut] in
            let resultModel = sut?.model(at: 0)
            XCTAssertEqual(resultModel, expected)
            expectation.fulfill()
        }
        sut.search()
        wait(for: [expectation], timeout: 0.1)
    }
}

extension SearchViewModelTests: SearchViewModelDelegate {
    func reload() {
        reloadCount += 1
        reloadHandler?()
    }
}
