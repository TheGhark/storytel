import XCTest
@testable import Storytel

final class SearchModelFactoryTests: XCTestCase {
    var sut: SearchModelFactory!

    override func setUp() {
        super.setUp()
        sut = .init()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_model() {
        let item: Item = .sample(
            title: "Harry Potter and the Order of the Phoenix",
            authors: [.sample(name: "J.K. Rowlin")],
            narrators: [.sample(name: "Stephen Fry")],
            cover: .sample(url: "https://www.storytel.com/images/9781781102404/640x640/cover.jpg")
        )
        let expected = SearchResultModel(
            imageUrl: item.cover.url,
            title: item.title,
            authors: "by J.K. Rowlin",
            narrators: "with Stephen Fry"
        )
        let model = sut.model(with: item)
        XCTAssertEqual(model, expected)
    }
}
